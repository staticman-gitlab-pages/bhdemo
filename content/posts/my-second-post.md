---
title: "My Second Post"
date: 2018-11-14T07:00:59+01:00
type: post
categories:
- example
tags:
- KaTeX
- sample
toc: true
draft: false
---

### First section

Another post to see how slug works.  $\KaTeX$ inline and display
equations.

#### First subsection

<div>
\[ f(n) = \sum_{k = 1}^\infty g(k) \]
</div>

#### Second subsection

$$\Omega = \bigcap_n A_n,$$

### Second section

Here's the sources.

#### subsection 2.1

    <div>
    \[ f(n) = \sum_{k = 1}^\infty g(k) \]
    </div>

#### subsection 2.2

    $$\Omega = \bigcap_n A_n,$$
