# Beautiful Hugo with Staticman comment reply
## Build your personal blog with comment replies for free on GitLab

View demo at https://staticman-gitlab-pages.frama.io/bhdemo ([source][3])

## Simple setup

1. Clone this repo

       git clone --recurse-submodules https://framagit.org/staticman-gitlab-pages/bhdemo.git <your-site-name>

1. Start a new project on GitLab named as `<your-site-name>`.
2. Remove existing comments under the folder `data` and the file `LICENSE`.
0. View the [first section of this Staticman setup guide][smguide] on GitHub.
3. Add your Framagit bot as a "**developer**" for your project.
3. Modify the following fields in the Hugo config file `config.toml`.  You may comment out stuff by `#`.

       baseURL = "https://<username>.frama.io"
       title = "Your title"

       [Params]
         subtitle = "Your subtitle"

       [Params.staticman]
         api = "https://{your-api-instance}/v3/entry/gitlab/<username>/<username>.frama.io/master/comments"

       [Author]
         name = "Your name"
         website = "https://example.com"
    If this is your project page, use the following parameters instead.

       baseURL = "https://<username>.frama.io/<your-project>"

       [Params.staticman]
         api = "https://{your-api-instance}/v3/entry/gitlab/<username>/<your-project>/master/comments"

4. Either remove the [reCAPTCHA][6] config or *change the parameters below with your own site key and secret*.  (You may apply your personal ones with your Google account.)

       [Params.staticman.recaptcha]
         sitekey = "6Lcv8G8UAAAAAEqV1Y-XEPum00C_DxhD6O--qkFo"
         secret = "p5uHlH9hCqp...33F1WaIYuwNw=="

5. Remove these lines at the bottom of `config.toml`.

       [[menu.main]]
         name = "Source"
         url = "https://framagit.org/staticman-gitlab-pages/bhdemo"
         weight = 2

8. Edit the bottom of `staticman.yml` corresponding to (6).
9. Set up either moderation or comments _without_ approval.
    - If you want to approve every comment before it's published to your blog,
    set `moderation: true`.
    - Otherwise, assuming that you are on a protected branch (such as `master`),
    you'll need to go to **Settings → Repository → Protected Branches** and
    grant your Framagit bot permission to push against the branch.

Write new posts.

1. Execute the command `hugo new posts/<your-filename>.md`
2. Edit `content/posts/<your-filename>.md`
3. Save everything with `git add .`
4. Commit with `git commit`.  Add your commit message.
5. Publish with `git push -u origin master`

  [3]: https://framagit.org/staticman-gitlab-pages/bhdemo
  [6]: https://www.google.com/recaptcha/intro/v3.html

## Credits

1. [Beautiful Hugo][bh] by Michael Romero
2. [Staticman][sm] by Eduardo Bouças
3. [Staticman PR 219][sm219] by Nicolas Tsim
4. [Network Hobo][nb] by Dan C Williams

[bh]: https://github.com/halogenica/beautifulhugo
[sm]: https://github.com/eduardoboucas/staticman
[sm219]: https://github.com/eduardoboucas/staticman/pull/219
[nb]: https://github.com/dancwilliams/networkhobo
[smguide]: https://github.com/pacollins/hugo-future-imperfect-slim/wiki/staticman.yml#setting-up-your-own-staticman-api-instance-on-heroku
